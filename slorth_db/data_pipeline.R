library(dplyr)
library(Hmisc)

data_dir = '/its/home/gb293/phd/data/slorth'
organism_code_key = c('human'='9606', 'yeast'='559292', 'fly'='7227', 'worm'='6239', 'pombe'='284812', 'mouse' = '10090')
orgs = c('Human' = 'H. sapiens', 'Yeast' = 'S. cerevisiae', 'Worm' = 'C. elegans', 'Fly' = 'D. melanogaster', 'Pombe' =  'S. pombe')

gi <- c('Synthetic Lethality', 'Negative Genetic')
gi_tag <- 'sl'
tag <- 'v2-2'
# source('biogrid_processing.R') # probably already done
source('process_prediction_file.R')

gi <- c('Dosage Lethality')
gi_tag <- 'sdl'
source('biogrid_processing.R')
source('process_prediction_file.R')


source('combine_datasets.R')
source('process_gene_list.R')
