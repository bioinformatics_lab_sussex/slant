
combine_data <- function(combined_pairs, organism, sum_score=F) {
  print('Combine and format pairs')
  print(organism)
  

  combined_pairs %>%
    na.omit()
  all_pairs <- combined_pairs %>% 
    rowwise() %>%
    mutate(gene1=ifelse(gene1=='', id1, gene1),
           gene2=ifelse(gene2=='', id2, gene2)) %>%
    as.data.frame()   ######## Fill in missing genes where missing in id_map file
  
  if(sum_score == T) {
  distinct_pairs <- all_pairs %>% 
    group_by(gene1, gene2, id1, id2, type, organism) %>%
    summarise(source = paste(source, collapse=', '), refs = paste(refs, collapse=''), score_add = sum(score)) %>%
    as.data.frame() %>%
    distinct()
  } 
  else {
    distinct_pairs <- all_pairs %>%
      distinct()
  }
  
    
  write.table(distinct_pairs, sprintf('%s/webapp_data/%s_pairs_%s.csv', data_dir, tolower(organism), tag), sep='\t', quote=F, row.names=F, col.names = F)
  return(distinct_pairs)
}

organism = 'Human'
bg_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_biogrid_pairs_%s.csv', data_dir, tolower(organism), 'sl'), sep='\t', header=T, stringsAsFactors = F)
slorth_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_prediction_slorth_%s_%s.csv', data_dir, tolower(organism), 'sl', tag), sep='\t', header=T, stringsAsFactors = F, quote = '')
bg_pairs_sdl <- read.table(sprintf('%s/webapp_data/%s_biogrid_pairs_%s.csv', data_dir, tolower(organism), 'sdl'), sep='\t', header=T, stringsAsFactors = F)
slorth_pairs_sdl <- read.table(sprintf('%s/webapp_data/%s_prediction_slorth_%s_%s.csv', data_dir, tolower(organism), 'sdl', tag), sep='\t', header=T, stringsAsFactors = F, quote = '')
combined_pairs <- rbind(bg_pairs_ssl, slorth_pairs_ssl, bg_pairs_sdl, slorth_pairs_sdl)
h = combine_data(combined_pairs, organism)
head(h)

organism = 'Yeast'
bg_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_biogrid_pairs_%s.csv', data_dir, tolower(organism), 'sl'), sep='\t', header=T, stringsAsFactors = F)
slorth_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_prediction_slorth_%s_%s.csv', data_dir, tolower(organism), 'sl', tag), sep='\t', header=T, stringsAsFactors = F, quote = '')
bg_pairs_sdl <- read.table(sprintf('%s/webapp_data/%s_biogrid_pairs_%s.csv', data_dir, tolower(organism), 'sdl'), sep='\t', header=T, stringsAsFactors = F)
slorth_pairs_sdl <- read.table(sprintf('%s/webapp_data/%s_prediction_slorth_%s_%s.csv', data_dir, tolower(organism), 'sdl', tag), sep='\t', header=T, stringsAsFactors = F, quote = '')
combined_pairs <- rbind(bg_pairs_ssl, slorth_pairs_ssl, bg_pairs_sdl, slorth_pairs_sdl)
combine_data(combined_pairs, organism, T)

organism = 'Fly'
bg_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_biogrid_pairs_%s.csv', data_dir, tolower(organism), 'sl'), sep='\t', header=T, stringsAsFactors = F)
slorth_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_prediction_slorth_%s_%s.csv', data_dir, tolower(organism), 'sl', tag), sep='\t', header=T, stringsAsFactors = F, quote = '')
combined_pairs <- rbind(bg_pairs_ssl, slorth_pairs_ssl)
combine_data(combined_pairs, organism)

organism = 'Worm'
bg_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_biogrid_pairs_%s.csv', data_dir, tolower(organism), 'sl'), sep='\t', header=T, stringsAsFactors = F)
slorth_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_prediction_slorth_%s_%s.csv', data_dir, tolower(organism), 'sl', tag), sep='\t', header=T, stringsAsFactors = F, quote = '')
combined_pairs <- rbind(bg_pairs_ssl, slorth_pairs_ssl)
combine_data(combined_pairs, organism)

organism = 'Pombe'
bg_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_biogrid_pairs_%s.csv', data_dir, tolower(organism), 'sl'), sep='\t', header=T, stringsAsFactors = F)
slorth_pairs_ssl <- read.table(sprintf('%s/webapp_data/%s_prediction_slorth_%s_%s.csv', data_dir, tolower(organism), 'sl', tag), sep='\t', header=T, stringsAsFactors = F, quote = '')
combined_pairs <- rbind(bg_pairs_ssl, slorth_pairs_ssl)
combine_data(combined_pairs, organism, T)




