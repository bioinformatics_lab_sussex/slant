# slant v1 #

The source code provided was used to create the predictions available via http://slorth.biochem.sussex.ac.uk/.

For support contact: gb293@sussex.ac.uk 

The SLant script tested using R 3.4.0. 

# Installation #

This script requires a data directory with some primary data files. The data directlry structure and files is available as a zip file at:

http://slorth.biochem.sussex.ac.uk/download/slant_data_dir.tar.gz (688MB)

Once you have downloaded this data directory edit the pipeline.R script and update data_dir with the location of the data on your system.

Once this is in place you should be able to run the whole programme using rscript pipeline.R. This should result in validation and predictions being added to the results directory in the new data_dir. 

# Options #

pipeline.R includes options for changing the type of genetic interaction being predicted under gi. Note this can be a vector of existing types of genetic interaction as classified by BioGRID.

A number of options appear throughout analysis/analysis.R to change how you want resulting files to be labelled and which features you want inlcuded in each model. Options also exist for the ratio of SSL to non-SSL observations etc.

# Other analysis #

A number of options appear throughout analysis/analysis.R depending on how you want resulting files to be labelled and which features you want inlcuded in each model. Options also exist for the ratio of SSL to non-SSL observations etc.

