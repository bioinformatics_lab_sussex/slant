graph
[
  Creator "Gephi"
  directed 0
  node
  [
    id 13
    label "13"
    graphics
    [
      x -74.36534
      y 48.935467
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "protein binding"
  ]
  node
  [
    id 15
    label "15"
    graphics
    [
      x -101.53018
      y 3.5492501
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "nucleus"
  ]
  node
  [
    id 16
    label "16"
    graphics
    [
      x -35.012703
      y 32.969112
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "nucleoplasm"
  ]
  node
  [
    id 18
    label "18"
    graphics
    [
      x -69.50079
      y 3.2229059
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "cytoplasm"
  ]
  node
  [
    id 21
    label "21"
    graphics
    [
      x -53.63734
      y 24.24672
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "plasma membrane"
  ]
  node
  [
    id 23
    label "23"
    graphics
    [
      x -51.595646
      y 46.863777
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "focal adhesion"
  ]
  node
  [
    id 49
    label "49"
    graphics
    [
      x -61.11659
      y -19.984976
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of gene expression"
  ]
  node
  [
    id 120
    label "120"
    graphics
    [
      x -102.163574
      y 23.213282
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "MAPK cascade"
  ]
  node
  [
    id 137
    label "137"
    graphics
    [
      x -92.08426
      y 88.0696
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "ATP binding"
  ]
  node
  [
    id 139
    label "139"
    graphics
    [
      x -56.56858
      y 3.0119216
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "cytosol"
  ]
  node
  [
    id 146
    label "146"
    graphics
    [
      x -94.71294
      y 53.575935
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of cell proliferation"
  ]
  node
  [
    id 156
    label "156"
    graphics
    [
      x -79.1292
      y -13.936076
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "membrane"
  ]
  node
  [
    id 183
    label "183"
    graphics
    [
      x -112.85077
      y 56.997402
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "Fc-epsilon receptor signaling pathway"
  ]
  node
  [
    id 270
    label "270"
    graphics
    [
      x -357.63602
      y 478.07828
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "NA"
  ]
  node
  [
    id 273
    label "273"
    graphics
    [
      x -137.11252
      y 52.882378
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "negative regulation of cell differentiation"
  ]
  node
  [
    id 289
    label "289"
    graphics
    [
      x -70.91772
      y 86.79743
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of protein phosphorylation"
  ]
  node
  [
    id 299
    label "299"
    graphics
    [
      x 496.82916
      y -179.57898
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "nucleolus"
  ]
  node
  [
    id 300
    label "300"
    graphics
    [
      x -81.399155
      y 66.394844
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "mitochondrion"
  ]
  node
  [
    id 309
    label "309"
    graphics
    [
      x -138.77713
      y 63.878838
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "epidermal growth factor receptor signaling pathway"
  ]
  node
  [
    id 330
    label "330"
    graphics
    [
      x 209.77316
      y 10.824096
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "actin cytoskeleton organization"
  ]
  node
  [
    id 338
    label "338"
    graphics
    [
      x 322.5481
      y 172.49916
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "extrinsic component of cytoplasmic side of plasma membrane"
  ]
  node
  [
    id 418
    label "418"
    graphics
    [
      x -28.690283
      y 6.8636456
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "GTPase activity"
  ]
  node
  [
    id 420
    label "420"
    graphics
    [
      x -50.479668
      y 90.015396
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "GTP binding"
  ]
  node
  [
    id 626
    label "626"
    graphics
    [
      x -77.36533
      y 25.043444
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "protein complex binding"
  ]
  node
  [
    id 638
    label "638"
    graphics
    [
      x -349.95135
      y 224.8793
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "membrane raft"
  ]
  node
  [
    id 719
    label "719"
    graphics
    [
      x -105.87643
      y 33.450684
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of MAP kinase activity"
  ]
  node
  [
    id 763
    label "763"
    graphics
    [
      x -30.110119
      y 74.074234
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "axon guidance"
  ]
  node
  [
    id 795
    label "795"
    graphics
    [
      x 93.18685
      y -31.280056
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "liver development"
  ]
  node
  [
    id 824
    label "824"
    graphics
    [
      x -112.455246
      y 84.36664
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "regulation of protein stability"
  ]
  node
  [
    id 832
    label "832"
    graphics
    [
      x -59.02498
      y 69.85651
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "ERBB2 signaling pathway"
  ]
  node
  [
    id 842
    label "842"
    graphics
    [
      x -130.67479
      y 30.795622
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "negative regulation of neuron apoptotic process"
  ]
  node
  [
    id 850
    label "850"
    graphics
    [
      x -80.15538
      y 29.89204
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "leukocyte migration"
  ]
  node
  [
    id 886
    label "886"
    graphics
    [
      x 188.38895
      y 510.94333
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "cytokine-mediated signaling pathway"
  ]
  node
  [
    id 909
    label "909"
    graphics
    [
      x 347.48077
      y 1.8879133
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "stimulatory C-type lectin receptor signaling pathway"
  ]
  node
  [
    id 912
    label "912"
    graphics
    [
      x -9.879222
      y 364.0699
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "Ras protein signal transduction"
  ]
  node
  [
    id 913
    label "913"
    graphics
    [
      x -30.214678
      y 56.090946
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "visual learning"
  ]
  node
  [
    id 915
    label "915"
    graphics
    [
      x 190.40012
      y -85.268585
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "GMP binding"
  ]
  node
  [
    id 916
    label "916"
    graphics
    [
      x 456.31064
      y -106.43203
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "GDP binding"
  ]
  node
  [
    id 917
    label "917"
    graphics
    [
      x -31.321299
      y 121.23482
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "forebrain astrocyte development"
  ]
  node
  [
    id 918
    label "918"
    graphics
    [
      x 424.62396
      y -173.47928
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "LRR domain binding"
  ]
  node
  [
    id 919
    label "919"
    graphics
    [
      x -243.60437
      y 73.05322
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "regulation of synaptic transmission, GABAergic"
  ]
  node
  [
    id 920
    label "920"
    graphics
    [
      x 69.85843
      y -363.8095
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of Rac protein signal transduction"
  ]
  node
  [
    id 921
    label "921"
    graphics
    [
      x -115.476685
      y 6.231073
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "social behavior"
  ]
  node
  [
    id 922
    label "922"
    graphics
    [
      x -54.074184
      y 162.48329
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "endocrine signaling"
  ]
  node
  [
    id 924
    label "924"
    graphics
    [
      x 369.0642
      y 399.5473
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "regulation of long-term neuronal synaptic plasticity"
  ]
  node
  [
    id 925
    label "925"
    graphics
    [
      x -374.45255
      y -99.52795
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "homeostasis of number of cells within a tissue"
  ]
  node
  [
    id 926
    label "926"
    graphics
    [
      x 215.624
      y -205.63358
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of nitric-oxide synthase activity"
  ]
  node
  [
    id 927
    label "927"
    graphics
    [
      x 211.79669
      y -11.353236
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of NF-kappaB transcription factor activity"
  ]
  node
  [
    id 928
    label "928"
    graphics
    [
      x -264.93988
      y -343.81564
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "striated muscle cell differentiation"
  ]
  node
  [
    id 929
    label "929"
    graphics
    [
      x 301.4823
      y 194.88612
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "response to glucocorticoid"
  ]
  node
  [
    id 930
    label "930"
    graphics
    [
      x -147.57294
      y 401.2237
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "response to mineralocorticoid"
  ]
  node
  [
    id 931
    label "931"
    graphics
    [
      x -273.6687
      y 218.61441
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "epithelial tube branching involved in lung morphogenesis"
  ]
  node
  [
    id 1094
    label "1094"
    graphics
    [
      x 350.61288
      y 98.70852
      z 0.0
      w 10.0
      h 10.0
      d 10.0
      fill "#000000"
    ]
    name "positive regulation of cellular senescence"
  ]
  edge
  [
    id 1282926
    source 13
    target 13
    value 361.0
  ]
  edge
  [
    id 1282928
    source 18
    target 13
    value 468.0
  ]
  edge
  [
    id 1282931
    source 21
    target 13
    value 276.0
  ]
  edge
  [
    id 1282933
    source 23
    target 13
    value 179.0
  ]
  edge
  [
    id 1282972
    source 15
    target 13
    value 326.0
  ]
  edge
  [
    id 1282974
    source 18
    target 15
    value 226.0
  ]
  edge
  [
    id 1282977
    source 21
    target 15
    value 159.0
  ]
  edge
  [
    id 1283018
    source 16
    target 13
    value 364.0
  ]
  edge
  [
    id 1283020
    source 18
    target 16
    value 235.0
  ]
  edge
  [
    id 1283023
    source 21
    target 16
    value 148.0
  ]
  edge
  [
    id 1283112
    source 18
    target 18
    value 159.0
  ]
  edge
  [
    id 1283115
    source 21
    target 18
    value 180.0
  ]
  edge
  [
    id 1284122
    source 49
    target 13
    value 211.0
  ]
  edge
  [
    id 1284124
    source 49
    target 18
    value 134.0
  ]
  edge
  [
    id 1286260
    source 120
    target 13
    value 211.0
  ]
  edge
  [
    id 1286262
    source 120
    target 18
    value 142.0
  ]
  edge
  [
    id 1286448
    source 137
    target 13
    value 173.0
  ]
  edge
  [
    id 1286450
    source 139
    target 13
    value 481.0
  ]
  edge
  [
    id 1286455
    source 183
    target 13
    value 169.0
  ]
  edge
  [
    id 1286484
    source 139
    target 15
    value 231.0
  ]
  edge
  [
    id 1286501
    source 139
    target 16
    value 238.0
  ]
  edge
  [
    id 1286518
    source 139
    target 18
    value 292.0
  ]
  edge
  [
    id 1286552
    source 139
    target 21
    value 166.0
  ]
  edge
  [
    id 1286668
    source 146
    target 13
    value 216.0
  ]
  edge
  [
    id 1286670
    source 146
    target 18
    value 138.0
  ]
  edge
  [
    id 1286838
    source 156
    target 13
    value 290.0
  ]
  edge
  [
    id 1286840
    source 156
    target 18
    value 190.0
  ]
  edge
  [
    id 1286841
    source 156
    target 139
    value 184.0
  ]
  edge
  [
    id 1288689
    source 273
    target 13
    value 154.0
  ]
  edge
  [
    id 1288788
    source 16
    target 15
    value 180.0
  ]
  edge
  [
    id 1289507
    source 270
    target 13
    value 134.0
  ]
  edge
  [
    id 1290197
    source 289
    target 13
    value 176.0
  ]
  edge
  [
    id 1290800
    source 300
    target 13
    value 243.0
  ]
  edge
  [
    id 1290920
    source 300
    target 15
    value 140.0
  ]
  edge
  [
    id 1291037
    source 299
    target 13
    value 136.0
  ]
  edge
  [
    id 1291100
    source 300
    target 18
    value 154.0
  ]
  edge
  [
    id 1291162
    source 300
    target 139
    value 143.0
  ]
  edge
  [
    id 1291222
    source 139
    target 139
    value 148.0
  ]
  edge
  [
    id 1291817
    source 309
    target 13
    value 164.0
  ]
  edge
  [
    id 1292537
    source 330
    target 13
    value 153.0
  ]
  edge
  [
    id 1292957
    source 338
    target 13
    value 157.0
  ]
  edge
  [
    id 1305340
    source 626
    target 13
    value 190.0
  ]
  edge
  [
    id 1307385
    source 638
    target 13
    value 159.0
  ]
  edge
  [
    id 1309966
    source 719
    target 13
    value 165.0
  ]
  edge
  [
    id 1310046
    source 156
    target 15
    value 150.0
  ]
  edge
  [
    id 1310159
    source 156
    target 16
    value 144.0
  ]
  edge
  [
    id 1318224
    source 763
    target 13
    value 164.0
  ]
  edge
  [
    id 1321390
    source 795
    target 13
    value 159.0
  ]
  edge
  [
    id 1321420
    source 832
    target 13
    value 172.0
  ]
  edge
  [
    id 1321424
    source 842
    target 13
    value 169.0
  ]
  edge
  [
    id 1321433
    source 850
    target 13
    value 163.0
  ]
  edge
  [
    id 1323265
    source 824
    target 13
    value 162.0
  ]
  edge
  [
    id 1325933
    source 886
    target 13
    value 155.0
  ]
  edge
  [
    id 1328701
    source 418
    target 13
    value 173.0
  ]
  edge
  [
    id 1328705
    source 420
    target 13
    value 185.0
  ]
  edge
  [
    id 1330046
    source 909
    target 13
    value 159.0
  ]
  edge
  [
    id 1330056
    source 912
    target 13
    value 159.0
  ]
  edge
  [
    id 1330059
    source 913
    target 13
    value 170.0
  ]
  edge
  [
    id 1330062
    source 915
    target 13
    value 150.0
  ]
  edge
  [
    id 1330063
    source 916
    target 13
    value 152.0
  ]
  edge
  [
    id 1330065
    source 917
    target 13
    value 151.0
  ]
  edge
  [
    id 1330067
    source 918
    target 13
    value 150.0
  ]
  edge
  [
    id 1330070
    source 919
    target 13
    value 155.0
  ]
  edge
  [
    id 1330072
    source 920
    target 13
    value 150.0
  ]
  edge
  [
    id 1330073
    source 921
    target 13
    value 162.0
  ]
  edge
  [
    id 1330074
    source 922
    target 13
    value 150.0
  ]
  edge
  [
    id 1330081
    source 924
    target 13
    value 151.0
  ]
  edge
  [
    id 1330082
    source 925
    target 13
    value 156.0
  ]
  edge
  [
    id 1330084
    source 926
    target 13
    value 153.0
  ]
  edge
  [
    id 1330085
    source 927
    target 13
    value 153.0
  ]
  edge
  [
    id 1330086
    source 928
    target 13
    value 154.0
  ]
  edge
  [
    id 1330087
    source 929
    target 13
    value 152.0
  ]
  edge
  [
    id 1330088
    source 930
    target 13
    value 152.0
  ]
  edge
  [
    id 1330089
    source 931
    target 13
    value 151.0
  ]
  edge
  [
    id 1330090
    source 1094
    target 13
    value 150.0
  ]
]
