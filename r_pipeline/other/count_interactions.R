library(ggplot2)
library(ggthemes)
library(ggraph)
library(igraph)
library(dplyr)


data_dir <- '/home/g/gb/gb293/phd/data/slorth' 
organism_code_key = c('human'='9606', 'yeast'='4932', 'fly'='7227', 'worm'='6239', 'pombe'='4896')

count_p <- function(organism_name) {
  organism_code = organism_code_key[organism_name]
  interactions <- read.table(sprintf('%s/interactions/%s.protein.links.detailed.v10.txt', data_dir, organism_code), sep = ' ', header=T, stringsAsFactors = F)
  interactions <- interactions %>% 
    filter(experimental > 800)
    
  nrow(interactions)
}

count_p('human')
count_p('yeast')
count_p('worm')
count_p('fly')
count_p('pombe')
